const webpack = require("webpack");
const bundlesObj = require("./src/shared/navigate/bundles");
const NODE_ENV = process.env.NODE_ENV || 'development';
const bundles = Object.values(bundlesObj);

let clientConfigs = [];


if (NODE_ENV === 'production') {
    clientConfigs = bundles.map(bundleName => ({
        entry: "./src/browser/index.js",
        output: {
            path: __dirname + '/public',
            publicPath: '/',
            filename: `bundle.${bundleName}.js`,
            chunkFilename: `chunk.${bundleName}.[id].[chunkhash:8].js`,
        },
        devtool: "cheap-module-source-map",
        module: {
            rules: [
                {
                    test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                    loader: "file-loader",
                    options: {
                        name: "public/media/[name].[ext]",
                        publicPath: url => url.replace(/public/, "")
                    }
                },
                {
                    test: /js$/,
                    exclude: /(node_modules)/,
                    loader: "babel-loader",
                    query: { presets: ["react-app"] }
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                "global.IS_SERVER": false,
                "process.env": {
                    'NODE_ENV': JSON.stringify(NODE_ENV)
                },
                "global.BUNDLE_NAME": JSON.stringify(bundleName)
            }),
            new webpack.optimize.UglifyJsPlugin({
                beautify: true,
                uglifyOptions: {
                    compress: false
                }
            })
        ]
    }));
} else {
    clientConfigs = [{
        entry: "./src/browser/index.js",
        output: {
            path: __dirname + '/public',
            publicPath: '/',
            filename: 'bundle.js',
            chunkFilename: 'chunk.[id].[chunkhash:8].js',
        },
        devtool: "cheap-module-source-map",
        module: {
            rules: [
                {
                    test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                    loader: "file-loader",
                    options: {
                        name: "public/media/[name].[ext]",
                        publicPath: url => url.replace(/public/, "")
                    }
                },
                {
                    test: /js$/,
                    exclude: /(node_modules)/,
                    loader: "babel-loader",
                    query: { presets: ["react-app"] }
                }
            ]
        },
        plugins: [
            new webpack.DefinePlugin({
                "global.IS_SERVER": true,
            })
        ]
    }];
}


const serverConfig = {
    entry: "./src/server/index.js",
    target: "node",
    output: {
        path: __dirname,
        filename: "server.js",
        libraryTarget: "commonjs2"
    },
    devtool: "cheap-module-source-map",
    module: {
        rules: [
            {
                test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: "file-loader",
                options: {
                    name: "public/media/[name].[ext]",
                    publicPath: url => url.replace(/public/, ""),
                    emit: false
                }
            },
            {
                test: /js$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
                query: { presets: ["react-app"] }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "global.GENTLY": false,
            "global.IS_SERVER": true
        })
    ]
};

module.exports = clientConfigs.concat([serverConfig]);
