import {
    RESET_ALBUM, FETCH_ALBUM_REQUEST, FETCH_ALBUM_SUCCESS, FETCH_ALBUM_FAILURE,
    RESET_PHOTOS, FETCH_PHOTOS_REQUEST, FETCH_PHOTOS_SUCCESS, FETCH_PHOTOS_FAILURE,
    FETCH_WEDDINGS_REQUEST, FETCH_WEDDINGS_SUCCESS, FETCH_WEDDINGS_FAILURE,
    FETCH_MEDIA_REQUEST, RESET_MEDIA, FETCH_MEDIA_SUCCESS, FETCH_MEDIA_FAILURE, UPDATE_MEDIA_FILTER,
    FETCH_MY_ALBUMS_SUCCESS, FETCH_MY_ALBUM_SUCCESS,
    FETCH_MY_MEDIAS_SUCCESS,
} from './../constants/';

// Action Creators Photos
export const resetPhotos = () => ({ type: RESET_PHOTOS });
export const requestPhotos = () => ({ type: FETCH_PHOTOS_REQUEST });
export const receivedPhotos = payload => ({ type: FETCH_PHOTOS_SUCCESS, payload });
export const photosError = () => ({ type: FETCH_PHOTOS_FAILURE });

// Action Creators Weddings
export const requestWeddings = () => ({ type: FETCH_WEDDINGS_REQUEST });
export const receivedWeddings = payload => ({ type: FETCH_WEDDINGS_SUCCESS, payload });
export const weddingsError = () => ({ type: FETCH_WEDDINGS_FAILURE });

// Action Creators Album
export const resetAlbum = () => ({ type: RESET_ALBUM });
export const requestAlbum = () => ({ type: FETCH_ALBUM_REQUEST });
export const receivedAlbum = payload => ({ type: FETCH_ALBUM_SUCCESS, payload });
export const albumError = () => ({ type: FETCH_ALBUM_FAILURE });

// Action Creators Photos
export const requestMedia = () => ({ type: FETCH_MEDIA_REQUEST });
export const receivedMedia = payload => ({ type: FETCH_MEDIA_SUCCESS, payload });
export const resetMedia = () => ({ type: RESET_MEDIA });
export const updateMediaFilter = (payload) => ({ type: UPDATE_MEDIA_FILTER, payload });
export const mediaError = () => ({ type: FETCH_MEDIA_FAILURE });

// Action Creators my

export const receivedMyAlbums = payload => ({ type: FETCH_MY_ALBUMS_SUCCESS, payload });
export const receivedMyMedia = payload => ({ type: FETCH_MY_MEDIAS_SUCCESS, payload });
export const receivedMyAlbum = payload => ({ type: FETCH_MY_ALBUM_SUCCESS, payload });
