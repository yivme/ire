import { fromUrlHelper } from '../routingHelper.js';

describe('Test routingHelper', () => {
    it('fromUrlHelper', () => {
        expect(fromUrlHelper('/photos/')).toEqual('photos');
        expect(fromUrlHelper('/photos/popular/')).toEqual('popular_photos');
    });
});