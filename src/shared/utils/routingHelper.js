import qs from 'query-string';
import { matchPath } from 'react-router-dom';
import { modalRoutes } from '../navigate/routes';

const FROM_URL_MAP = {
    photos: '/photos/',
    popular_photos: '/photos/popular/',
    discussed_photos: '/photos/discussed/',
    best_photos: '/photos/best/',
    popular_weddings: '/weddings/popular/',
    discussed_weddings: '/weddings/discussed/',
    profile: '/profile/albums/',
    weddings: '/weddings/',
};

const URL_FROM_MAP = {};
for (let key in FROM_URL_MAP) {
    URL_FROM_MAP[FROM_URL_MAP[key]] = key
}

/**
 * Преобразует url в параметр from
 * @param url
 */
export const fromUrlHelper = url => {
    return URL_FROM_MAP[url];
};

/**
 * Преобразует параметр from в url
 * @param from
 * @param url
 * @returns {*}
 */
export const urlFromHelper = (from, url = '') => {
    if (from === 'album') {
        const albumId = url.split('/')[2];
        return `/profile/albums/${albumId}/`
    }

    return FROM_URL_MAP[from];
};

/**
 * Узнает есть, или была на предыдущем шаге, в ардесе модалка
 * @param nextURl
 * @param currentURl
 * @returns {*}
 */
export const isModalMediaItemInURLs = (nextURl, currentURl) => {
    return matchPath(nextURl, modalRoutes[0])
        || matchPath(currentURl, modalRoutes[0]);
};

/**
 FROM: {
  ALBUM_LIST: 'profile',
  ALBUM: 'album',
  PHOTOS: ['new', 'popular', 'discussed', 'best', 'favorites', 'my'],
  WEDDING: ['wedding_new', 'wedding_popular', 'wedding_discussed', 'wedding_chosen'], //??
},

 //за медией = from => [profile, album]
 ${Locator.apiUrl()}/media/${mediaID}/neighbor;

 //mediaFeed from=> ['new', 'popular', 'discussed', 'best', 'favorites', 'my']
 ${Locator.apiUrl()}/mediaFeed/${mediaID}/neighbor;

 prev_limit=1&next_limit=1&type=${type}

 */

const queryDefault = {
    prev_limit: 1,
    next_limit: 1
};

const definePhoto = (from) => {
    if (from === 'photos') {
        return {
            source: 'mediaFeed',
            query: { type: 'new' }
        }
    }

    const index = from.indexOf('_photos');
    if (index !== -1) {
        return {
            source: 'mediaFeed',
            query: { type: from.substr(0, index) }
        }
    }
};

const defineProfile = from => {
    return {
        source: 'media',
        query: { order: '-id', type: 'all' }
    }
};

export const defineMediaNeighbor = (from = '') => {
    let result = { source: 'media', query: { type: 'album', order: 'position' }};

    if (~from.indexOf('photos')) {
        result = definePhoto(from);
    }

    if (~from.indexOf('profile')) {
        result = defineProfile(from);
    }

    if (~from.indexOf('album')) {

    }

    return {
        source: result.source,
        query: qs.stringify({ ...queryDefault, ...result.query })
    };
};
