import React from "react";
import { Route } from "react-router-dom";
import Navigate from './navigate/navigate';

const App = () => {
    return (
        <Route component={Navigate} />
    );
};

export default App;
