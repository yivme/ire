module.exports = {
    ALBUMS: 'albums',
    PHOTOS: 'photos',
    WEDDINGS: 'weddings',
    PROFILE: 'profile',
    MAIN: 'main'
};