import React, { Component } from 'react';
import { Switch, Route, Link, matchPath } from 'react-router-dom';
import qs from 'query-string';
import routes, {
    modalRoutes,
    getMediaItemByCurrentPath,
    getAlbumModalByCurrentPath
}  from './routes';
import { urlFromHelper } from '../utils/routingHelper';

class Navigate extends Component {

    constructor(props) {
        super(props);

        const search = qs.parse(props.location.search);

        this.state = {
            from: search.from || null
        };
    }

    componentWillReceiveProps(newProps) {
        const search = qs.parse(newProps.location.search);
        const mediaModalRouteParams = matchPath(newProps.location.pathname, modalRoutes[0]);

        this.setState({
            from: search.from || null,
            mediaModalRouteParams
        });
    }

    renderSwitch() {
        const { location } = this.props;
        let currentRoute = null;

        if (this.state.from) {
            //from
            const routeFrom = routes.find(route => {
                return matchPath(
                    urlFromHelper(this.state.from, location.pathname),
                    route
                )
            });

            currentRoute = routeFrom && { ...routeFrom, path: location.pathname };
        } else {
            const mediaItem = getMediaItemByCurrentPath(location.pathname);
            const albumRoute = mediaItem && routes.find(route => route.name === 'album');

            currentRoute = albumRoute && {
                ...albumRoute,
                path: location.pathname
            };
        }

        return (
            <Switch>
                {routes.map((route, i) => {
                    if (currentRoute && currentRoute.name === route.name) {
                        return <Route key={i} {...currentRoute} />
                    }

                    return <Route key={i} {...route} />
                })}
            </Switch>
        );
    }

    renderMediaList() {
        const { from, mediaModalRouteParams } = this.state;
        const { location } = this.props;
        const isFromWedding = from && ~from.indexOf('wedding');
        let albumModal;

        if (isFromWedding) {
            albumModal = getAlbumModalByCurrentPath(location.pathname);
        }

        if (isFromWedding && mediaModalRouteParams) {
            albumModal = modalRoutes.find(modalRoute => modalRoute.name === 'albumModal');
            albumModal = { ...albumModal, path: location.pathname }
        }

        return <Switch>
            {<Route {...albumModal}/>}
        </Switch>;
    }

    renderMediaItem() {
        const { location } = this.props;
        const mediaItem = getMediaItemByCurrentPath(location.pathname);

        return <Switch>
            {<Route {...mediaItem}/>}
        </Switch>;
    }

    render() {
        return (
            <div>
                <ul style={{ display: 'flex' }}>
                    <li style={{ padding: '20px' }}><Link to='/'>Главная</Link></li>
                    <li style={{ padding: '20px' }}><Link to='/photos/'>Фотопоток</Link></li>
                    <li style={{ padding: '20px' }}><Link to='/weddings/'>Свадьбы</Link></li>
                    <li style={{ padding: '20px' }}><Link to="/profile/albums/">Альбом пользователя</Link></li>
                </ul>
                {this.renderSwitch()}
                {this.renderMediaList()}
                {this.renderMediaItem()}
            </div>
        );
    }
}

export default Navigate;
