import { matchPath } from 'react-router-dom';
import Main from "../ui/pages/Main";
import MediaModal from '../ui/pages/MediaModal';
import AlbumModal from '../ui/pages/AlbumModal';
import Album from "../ui/pages/Album";
import Photos from "../ui/pages/Photos";
import Weddings from "../ui/pages/Weddings";
import ProfileAlbums from "../ui/pages/ProfileAlbums";
import ProfileAlbumsItem from "../ui/pages/ProfileAlbumsItem";
import bundles from './bundles';

export const modalRoutes = [
    {
        path: "/album/:albumId/:mediaId",
        exact: true,
        component: MediaModal,
        name: "mediaItem"
    },
    {
        path: "/album/:albumId",
        exact: true,
        component: AlbumModal,
        name: "albumModal"
    }
];

const routes = [
    {
        path: "/",
        exact: true,
        component: Main,
        name: "main",
        bundleName: bundles.MAIN,
    },
    {
        path: "/photos/",
        exact: true,
        component: Photos,
        name: "photos",
        bundleName: bundles.PHOTOS,
    },
    {
        path: "/photos/:type",
        exact: true,
        component: Photos,
        name: "photos_type",
        bundleName: bundles.PHOTOS,
    },
    {
        path: "/weddings/",
        exact: true,
        component: Weddings,
        name: "weddings",
        bundleName: bundles.WEDDINGS,
    },
    {
        path: "/profile/albums/",
        exact: true,
        component: ProfileAlbums,
        name: "profileAlbums",
        bundleName: bundles.PROFILE,
    },
    {
        path: "/profile/albums/:albumId",
        exact: true,
        component: ProfileAlbumsItem,
        name: "profileAlbumsItem",
        bundleName: bundles.PROFILE,
    },
    { // ought to be latest!
        path: "/album/:albumId/",
        exact: true,
        component: Album,
        name: "album",
        bundleName: bundles.ALBUMS,
    },
];

export const getMediaItemByCurrentPath = pathname => modalRoutes.find(route => (
    route.name === 'mediaItem' && matchPath(pathname, route))
);

export const getAlbumModalByCurrentPath = pathname => modalRoutes.find(route => (
    route.name === 'albumModal' && matchPath(pathname, route))
);

export default routes;
