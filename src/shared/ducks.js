
import {
    FETCH_PHOTOS_REQUEST,
    FETCH_PHOTOS_SUCCESS,
    RESET_PHOTOS,
    FETCH_WEDDINGS_SUCCESS,
    RESET_ALBUM,
    FETCH_ALBUM_SUCCESS,
    FETCH_MEDIA_SUCCESS,
    RESET_MEDIA,
    UPDATE_MEDIA_FILTER,
    FETCH_MY_ALBUMS_SUCCESS,
    FETCH_MY_MEDIAS_SUCCESS,
    FETCH_MY_ALBUM_SUCCESS
} from './constants';
import { defineMediaNeighbor } from './utils/routingHelper';

import {
    requestPhotos,
    receivedPhotos,
    receivedWeddings,
    receivedAlbum,
    receivedMedia,
    updateMediaFilter,
    receivedMyAlbums,
    receivedMyMedia,
    receivedMyAlbum,
} from './actions';
import request from "superagent";

// Reducer
export default function reducer(state = {}, action) {
    switch (action.type) {
        case RESET_PHOTOS:
            return { ...state, photos: null };

        case FETCH_PHOTOS_REQUEST:
            return { ...state, photos: null };

        case FETCH_PHOTOS_SUCCESS:
            return { ...state, photos: action.payload };

        case FETCH_WEDDINGS_SUCCESS:
            return { ...state, weddings: action.payload };

        case FETCH_ALBUM_SUCCESS:
            return { ...state, album: action.payload.album };

        case RESET_ALBUM:
            return { ...state, album: null };

        case FETCH_MEDIA_SUCCESS:
            return { ...state, media: action.payload };

        case RESET_MEDIA:
            return { ...state, media: null };

        case UPDATE_MEDIA_FILTER:
            return { ...state, mediaFilter: action.payload };

        case FETCH_MY_ALBUMS_SUCCESS:
            return { ...state, myAlbums: action.payload };

        case FETCH_MY_MEDIAS_SUCCESS:
            return { ...state, myMedias: action.payload };

        case FETCH_MY_ALBUM_SUCCESS:
            return { ...state, myAlbum: action.payload };

        default:
            return state;
    }
}

export const fetchPhotos = ({ type }) => (dispatch, getState) => {
    const feedType = type || 'new';
    const requestGet = request
        .get(`http://api.gorko.ru/api/v2/mediaFeed?media_type=photos&feed_type=${feedType}&per_page=30&preview_size=260x0x0`)
        .withCredentials();
    const { cookies } = getState();

    if (cookies) {
        requestGet.set('Cookie', `${cookies}; path=/; domain=.gorko.ru;`)
    }

    dispatch(requestPhotos());

    return requestGet
        .then((payload) => dispatch(receivedPhotos(payload.body)))
        .then(() => { dispatch(updateMediaFilter({ type: feedType })) })
        .catch(error => {
            console.log(error);
        });
};

export const fetchWeddings = () => (dispatch) => {
    return request.get('http://api.gorko.ru/api/v2/albums?embed=medias(limit:30)')
        .withCredentials()
        .then((payload) => dispatch(receivedWeddings(payload.body)))
        .catch(error => {
            console.log(error);
        });
};

export const fetchAlbum = (match) => (dispatch) => {
    const albumId = match.params.albumId;

    return request.get(`http://api.gorko.ru/api/v2/albums/${albumId}/?embed=medias(limit:30)&preview_size=260x0x0`)
        .withCredentials()
        .then((payload) => dispatch(receivedAlbum(payload.body)))
        .catch(error => {
            console.log(error);
        });
};

export const fetchMedia = (match) => (dispatch) => {
    const mediaId = match.params.mediaId;
    const { source, query } = defineMediaNeighbor(match.search.from);

    return request.get(`http://api.gorko.ru/api/v2/${source}/${mediaId}/neighbor?${query}`)
        .withCredentials()
        .then((payload) => dispatch(receivedMedia(payload.body)))
        .catch(error => {
            console.log(error);
        });
};

export const fetchMyAlbums = () => (dispatch, getState) => {
    const requestGet = request.get(`http://api.gorko.ru/api/v2/my/albums?per_page=30`)
        .withCredentials();

    const { cookies } = getState();

    if (cookies) {
        requestGet.set('Cookie', `${cookies}; path=/; domain=.gorko.ru;`)
    }

    return requestGet
        .then((payload) => dispatch(receivedMyAlbums(payload.body)))
        .catch(error => {
            console.log(error);
        });
};

export const fetchMyMedia = () => (dispatch, getState) => {
    const requestGet = request.get(`http://api.gorko.ru/api/v2/my/media?per_page=30&preview_size=260x0x0`)
        .withCredentials();

    const { cookies } = getState();

    if (cookies) {
        requestGet.set('Cookie', `${cookies}; path=/; domain=.gorko.ru;`)
    }

    return requestGet
        .then((payload) => dispatch(receivedMyMedia(payload.body)))
        .catch(error => {
            console.log(error);
        });
};

export const fetchMyAlbum = (match) => (dispatch) => {
    const albumId = match.params.albumId;

    return request.get(`http://api.gorko.ru/api/v2/albums/${albumId}/?embed=medias(limit:30)&preview_size=260x0x0`)
        .withCredentials()
        .then((payload) => dispatch(receivedMyAlbum(payload.body)))
        .catch(error => {
            console.log(error);
        });
};
