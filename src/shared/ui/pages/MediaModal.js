import React, { Component }from "react";
import { connect } from "react-redux";
import qs from 'query-string';
import { fetchMedia } from "../../ducks";
import { resetMedia } from '../../actions/'
import Media from '../components/media/Media';

class MediaPage extends Component {

    static initialAction(match) {
        return [fetchMedia(match)];
    }

    fetch(props) {
        const search = qs.parse(props.location.search);
        const actions = MediaPage.initialAction({ ...props.match, search });


        actions.map(action => {
            this.props.dispatch(action);
        });
    }

    componentDidMount() {
        if (!this.props.media) {
            this.fetch(this.props);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.url !== this.props.match.url) {
            this.props.dispatch(resetMedia());
            this.fetch(nextProps);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetMedia());
    }

    render() {
        return <Media { ...this.props } />;
    }
}

const mapStateToProps = state => ({
    media: state.media,
    mediaFilter: state.mediaFilter,
});


export default connect(mapStateToProps)(MediaPage);
