import React, { Component } from "react";
import { connect } from "react-redux";
import ProfileAlbum from '../components/profileAlbum/ProfileAlbum';
import { fetchMyAlbum } from '../../ducks';

class ProfileAlbumsItemPage extends Component {

    static initialAction(match) {

        return [
            fetchMyAlbum(match)
        ];
    }

    fetch(props) {
        const actions = ProfileAlbumsItemPage.initialAction({ ...props.match });

        actions.map(action => {
            props.dispatch(action);
        });
    }

    constructor(props) {
        super(props);

        this.state = {
            contentComponent: null
        };
    }

    componentWillMount() {
        if (global.IS_SERVER === true) {
            this.setState({
                contentComponent: require('../components/profileAlbum/ProfileAlbum').default
            });
        }

        if (global.IS_SERVER === false) {
            if (global.BUNDLE_NAME === 'profile') {
                this.setState({
                    contentComponent: require('../components/profileAlbum/ProfileAlbum').default
                });
            }

            if (global.BUNDLE_NAME !== 'profile') {
                System.import("../components/profileAlbum/ProfileAlbum")
                    .then(module => {
                        this.setState({
                            contentComponent: module.default
                        });
                    }).catch(err => {
                    console.log("Chunk loading failed");
                });
            }
        }
    }

    componentDidMount() {
        const { myAlbum } = this.props;
        if (!myAlbum) {
            this.fetch(this.props);
        }
    }

    render() {
        const { myAlbum } = this.props;
        const { contentComponent } = this.state;
        const ContentComponent = contentComponent;

        return ContentComponent
            ? <ContentComponent myAlbum={myAlbum} />
            : null;
    }
}

const mapStateToProps = state => ({
    myAlbum: state.myAlbum,
});

export default connect(mapStateToProps)(ProfileAlbumsItemPage);
