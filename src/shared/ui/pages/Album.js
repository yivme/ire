import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchAlbum } from "../../ducks";

class AlbumPage extends Component {

    static initialAction(match) {
        return [
            fetchAlbum(match)
        ];
    }

    constructor(props) {
        super(props);

        this.state = {
            contentComponent: null
        };
    }

    fetch() {
        const actions = AlbumPage.initialAction(this.props.match);

        actions.map(action => {
            this.props.dispatch(action);
        });
    }

    componentWillMount() {
        if (global.IS_SERVER === true) {
            this.setState({
                contentComponent: require('../components/album/Album').default
            });
        }

        if (global.IS_SERVER === false) {
            if (global.BUNDLE_NAME === 'albums') {
                this.setState({
                    contentComponent: require('../components/album/Album').default
                });
            }

            if (global.BUNDLE_NAME !== 'albums') {
                System.import("./../components/album/Album")
                    .then(module => {
                        this.setState({
                            contentComponent: module.default
                        });
                    }).catch(err => {
                    console.log("Chunk loading failed");
                });
            }
        }
    }

    componentDidMount() {
        if (!this.props.album) {
            this.fetch();
        }
    }

    render() {
        const { album } = this.props;
        const { contentComponent } = this.state;
        const ContentComponent = contentComponent;

        return ContentComponent
            ? <ContentComponent album={album} />
            : null;
    }
}

const mapStateToProps = state => ({
    album: state.album
});

export default connect(mapStateToProps)(AlbumPage);
