import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchWeddings } from "../../ducks";

class Weddings extends Component {

    static initialAction() {
        return [
            fetchWeddings()
        ];
    }

    constructor(props) {
        super(props);

        this.state = {
            contentComponent: null
        };
    }

    fetch(props) {
        const actionList = Weddings.initialAction(props);

        actionList.forEach(action => this.props.dispatch(action));
    }

    componentWillMount() {
        if (global.IS_SERVER === true) {
            this.setState({
                contentComponent: require('../components/weddings/WeddingsList').default
            });
        }

        if (global.IS_SERVER === false) {
            if (global.BUNDLE_NAME === 'weddings') {
                this.setState({
                    contentComponent: require('../components/weddings/WeddingsList').default
                });
            }

            if (global.BUNDLE_NAME !== 'weddings') {
                System.import("../components/weddings/WeddingsList")
                    .then(module => {
                        this.setState({
                            contentComponent: module.default
                        });
                    }).catch(err => {
                    console.log("Chunk loading failed");
                });
            }
        }
    }

    componentDidMount() {
        if (!this.props.weddings) {
            this.fetch(this.props)
        }
    }

    render() {
        const { weddings } = this.props;
        const { contentComponent } = this.state;
        const ContentComponent = contentComponent;

        return ContentComponent
            ? <ContentComponent weddings={weddings} />
            : null;
    }
}

const mapStateToProps = state => ({
    weddings: state.weddings
});

export default connect(mapStateToProps)(Weddings);