import React, { Component } from "react";
import { connect } from "react-redux";
import { resetAlbum } from '../../actions';
import { fetchAlbum } from "../../ducks";
import AlbumModal from "../components/albumModal/AlbumModal";

class AlbumModalPage extends Component {

    static initialAction(match) {
        return [
            fetchAlbum(match)
        ];
    }

    fetch(props) {
        const actions = AlbumModalPage.initialAction(props.match);

        actions.map(action => {
            props.dispatch(action);
        });
    }

    componentDidMount() {
        if (!this.props.album) {
            this.fetch(this.props);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetAlbum());
    }

    render() {
        return <AlbumModal { ...this.props } />;
    }
}

const mapStateToProps = state => ({
    album: state.album
});

export default connect(mapStateToProps)(AlbumModalPage);