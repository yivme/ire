import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchMyAlbums, fetchMyMedia } from '../../ducks';

class ProfileAlbumsPage extends Component {

    static initialAction(match) {
        return [
            fetchMyAlbums(match),
            fetchMyMedia(match)
        ];
    }

    constructor(props) {
        super(props);

        this.state = {
            contentComponent: null
        };
    }

    componentWillMount() {
        if (global.IS_SERVER === true) {
            this.setState({
                contentComponent: require('../components/profile/Profile').default
            });
        }

        if (global.IS_SERVER === false) {
            if (global.BUNDLE_NAME === 'profile') {
                this.setState({
                    contentComponent: require('../components/profile/Profile').default
                });
            }

            if (global.BUNDLE_NAME !== 'profile') {
                System.import("../components/profile/Profile")
                    .then(module => {
                        this.setState({
                            contentComponent: module.default
                        });
                    }).catch(err => {
                    console.log("Chunk loading failed");
                });
            }
        }
    }

    componentDidMount() {
        const { myAlbums, myMedias } = this.props;

        if (!myAlbums && !myMedias) {
            const actionList = ProfileAlbumsPage.initialAction();

            actionList.forEach(action => this.props.dispatch(action));
        }
    }

    render() {
        const { myAlbums, myMedias, match } = this.props;
        const { contentComponent } = this.state;
        const ContentComponent = contentComponent;

        return ContentComponent
            ? <ContentComponent
                myAlbums={myAlbums}
                myMedias={myMedias}
                match={match}
            />
            : null;
    }
}

const mapStateToProps = state => ({
    myAlbums: state.myAlbums,
    myMedias: state.myMedias,
});

export default connect(mapStateToProps)(ProfileAlbumsPage);
