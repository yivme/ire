import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPhotos } from "../../ducks";
import { resetPhotos } from '../../actions';
import { fromUrlHelper, isModalMediaItemInURLs } from '../../utils/routingHelper';

class PhotosPage extends Component {

    static initialAction(match) {
        return [fetchPhotos(match.params)];
    }

    constructor(props) {
        super(props);

        this.state = {
            contentComponent: null
        };
    }

    fetch(props) {
        const actions = PhotosPage.initialAction(props.match);

        actions.map(action => {
            props.dispatch(action);
        });
    }

    componentWillMount() {
        if (global.IS_SERVER === true) {
            this.setState({
                contentComponent: require('../components/photos/PhotosList').default
            });
        }

        if (global.IS_SERVER === false) {
            if (global.BUNDLE_NAME === 'photos') {
                this.setState({
                    contentComponent: require('../components/photos/PhotosList').default
                });
            }

            if (global.BUNDLE_NAME !== 'photos') {
                System.import("./../components/photos/PhotosList")
                    .then(module => {
                        this.setState({
                            contentComponent: module.default
                        });
                    }).catch(err => {
                    console.log("Chunk loading failed");
                });
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (!isModalMediaItemInURLs(nextProps.match.url, this.props.match.url)
            && nextProps.match.url !== this.props.match.url) {
            this.fetch(nextProps);
        }
    }

    componentDidMount() {
        const { photos } = this.props;

        if (!photos) {
            this.fetch(this.props);
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetPhotos());
    }

    render() {
        const { photos, match } = this.props;
        const fromParam = (fromUrlHelper(match.url));
        const { contentComponent } = this.state;
        const ContentComponent = contentComponent;

        return contentComponent
            ? <ContentComponent photos={photos} fromParam={fromParam} />
            : null;
    }
}

const mapStateToProps = state => ({
    photos: state.photos
});

export default connect(mapStateToProps)(PhotosPage);
