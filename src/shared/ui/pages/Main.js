import React, { Component } from "react";
import { Link } from "react-router-dom";
import Portal from '../components/portal/Portal';

class Main extends Component {
    render() {
        const button1 = <button>Open portal with pseudo modal</button>;

        return (<aside>
            <p>___</p>
            <div style={{ padding: '10px' }}><Link to="/album/4177019/">Albums 4177019</Link></div>

            <div>
                <Portal closeOnEsc closeOnOutsideClick openByClickOn={button1}>
                    <div className='modal'>
                        <h2>Pseudo Modal 2</h2>
                        <p>This react component is appended to the document body.</p>
                    </div>
                </Portal>
            </div>
        </aside>);
    }
}


export default Main;
