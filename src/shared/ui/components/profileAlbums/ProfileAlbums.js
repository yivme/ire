import React, { Component } from 'react';
import { Link } from "react-router-dom";


class ProfileAlbums extends Component {
    renderItemList() {
        const { myAlbums } = this.props;

        return myAlbums.albums.map(item => (
            <li key={item.id}>
                <Link to={`/profile/albums/${item.id}/`}>
                    {item.title}
                </Link>
            </li>
        ));
    }

    render() {
        const { myAlbums } = this.props;

        if (!myAlbums) {
            return <div>loading...</div>
        }

        return <div>
            Альбомы
                <ul>
                    {this.renderItemList()}
                </ul>
            </div>
        ;
    }
}

export default ProfileAlbums;
