import React, { Component } from 'react';
import Items from './Items'

class Album extends Component {
    render() {
        const { album } = this.props;

        return <div>
            zzz
                Album {album && album.title || '...'}
                <div>
                    {album && album.medias && <Items medias={album.medias} /> || '...'}
                </div>
            </div>;
    }
}

export default Album;
