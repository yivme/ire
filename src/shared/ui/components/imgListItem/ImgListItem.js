import React, { Component } from 'react';

class ImgListItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false
        };
    }

    componentDidMount() {
        this.setState({
            isLoaded: this.img.complete || this.img.height + this.img.width > 0
        });
    }

    handleLoad = () => {
        if (!this.state.loaded) {
            this.setState({ isLoaded: true });
        }
    };

    setRef = img => { this.img = img };

    render() {
        const { item } = this.props;
        const { isLoaded } = this.state;
        const size = item.preview_size.split('x');

        return <div style={{
            height:`${size[1]}px`,
            width:`${size[0]}px`,
            backgroundColor: 'gray'
        }}>
            <img
                style={{ visibility: isLoaded ? 'visible' : 'hidden' }}
                ref={this.setRef}
                onLoad={this.handleLoad}
                src={item.preview_url}
            />
        </div>
    }

}

export default ImgListItem;