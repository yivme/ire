import React, { Component } from 'react';
import { Link } from "react-router-dom";

class WeddingsList extends Component {

    renderMedia(list) {

        if (!list.length) {
            return null;
        }

        const shortList = list.slice(0, 5);
        const firstPhoto = shortList.shift();

        return <div style={{
            width: '462px',
            position: 'relative',
            margin: '25px'
        }}>
            <Link to={`/album/${firstPhoto.album_id}/?from=weddings`}>
                 <img
                     src={firstPhoto.preview_url} alt=""
                     width="230"
                     height="227"
                     style={{ float: "left" }}
                 />
                {shortList.map(photo => (
                    <img
                        key={photo.id}
                        src={photo.preview_url}
                        width="111"
                        height="111"
                        alt=""
                    />
                ))}
            </Link>
        </div>
    }

    renderList() {
        const { weddings } = this.props;

        return <ul style={{ display: 'flex', flexWrap: 'wrap' }}>
            {weddings.albums.map(item => (
                <li key={item.id}>
                    {item.title}
                    {this.renderMedia(item.medias)}
                </li>
            ))}
        </ul>;
    }

    render() {
        const { weddings } = this.props;
        let weddingsMarkup = 'loading...';

        if (weddings) {
            weddingsMarkup = this.renderList();
        }

        return <div>
            <div>weddings_p</div>
            {weddingsMarkup}
        </div>;
    }
}

export default WeddingsList;
