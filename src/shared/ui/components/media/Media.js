import React, { Component } from 'react';
import qs from 'query-string';
import { urlFromHelper } from '../../../utils/routingHelper.js';
import Portal from '../portal/Portal';
import Photo from './Photo';

class Media extends Component {

    componentDidMount() {
        this.portal.openPortal();
    }

    handleClose = e => {
        e.preventDefault();
        const { location, history, match } = this.props;
        const search = qs.parse(location.search);

        if (search && search.from) {
            if (search.from === 'weddings' && match.params.mediaId) {
                history.push(`/album/${match.params.albumId}/?from=weddings`);
            } else {
                history.push(urlFromHelper(search.from, location.pathname));
            }
        } else {
            history.push(`/album/${match.params.albumId}/`);
        }
    };

    render() {
        const { media, location } = this.props;
        const getParams = qs.parse(location.search);

        return (
            <Portal ref={portal => this.portal = portal}>
                <Photo
                    media={media}
                    from={getParams.from}
                    handleClose={this.handleClose}
                />
            </Portal>
        );
    }
}

export default Media;
