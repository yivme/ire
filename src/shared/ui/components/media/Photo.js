import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Portal from '../portal/Portal';

const t = text => (text);

class Photo extends Component {

    render() {
        const { media, from } = this.props;
        const button1 = <button>Open portal with pseudo modal</button>;

        if (!media) {
            return <span className='modal'>...loading</span>;
        }

        const { prev, next } = media;
        let prevSrc = prev.length > 0 && (`/album/${prev[prev.length - 1].album_id}/${prev[prev.length - 1].id}/`);
        let nextSrc = next.length > 0 && (`/album/${next[0].album_id}/${next[0].id}/`);

        prevSrc = prevSrc && from ? `${prevSrc}?from=${from}` : prevSrc;
        nextSrc = nextSrc && from ? `${nextSrc}?from=${from}` : nextSrc;

        return (<div className='modal'>
            <div>
                <Portal closeOnEsc closeOnOutsideClick openByClickOn={button1}>
                    <div className='modal'>
                        <h2>Фоторейтинг</h2>
                        <ul>
                            <li>1</li>
                            <li>2</li>
                            <li>3</li>
                        </ul>
                    </div>
                </Portal>
                {this.props.children}
            </div>

            <div
                className="ic-wrap-20-40 ** -sectPhoto-media"
                onClick={this.props.handleClose}
            >
                <i className="ic-close size20 ** icon-close" />
            </div>

            <div className="album-gallery-preview typeVideo">
                <img className="album-gallery-preview-img" id="" src={media.current.preview_url} alt=""/>
                {prevSrc && <Link
                    className="ic-wrap-20-80 ** -sectPhoto-media-l"
                    to={prevSrc}
                    replace
                >
                    <i className="ic-arrow size50 ** icon-arrow" role="button" tabIndex="0" />
                </Link>}

                {nextSrc && <Link
                    className="ic-wrap-20-80 ** -sectPhoto-media-r"
                    to={nextSrc}
                    replace
                >
                    <i className="ic-arrow size50 itemDirR ** icon-arrow" role="button" tabIndex="0" />
                </Link>}
                <i className="widget-iconPlay size100 ** icon-play" />
                <div className="widget-loader-wrap size40 typeGray">
                    <i className="widget-loader ** icon-loader" />
                </div>
            </div>
        </div>);
    }
}


export default Photo;
