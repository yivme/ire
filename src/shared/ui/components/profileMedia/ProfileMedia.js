import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import { Link } from "react-router-dom";
import ImgListItem from '../imgListItem/ImgListItem';

class ProfileMedia extends Component {

    constructor(params) {
        super(params);

        this.state = {
            isLayoutComplete: false
        };
    }

    renderItemList() {
        const { myMedias, fromParam } = this.props;
        return myMedias.media.map(item => {
            return <li key={item.id} style={{ padding: `10px` }}>
                <Link to={`/album/${item.album_id}/${item.id}/?from=${fromParam}`}>
                    <div>{item.original_size}</div>
                    <ImgListItem item={item} />
                </Link>
            </li>
        });
    }

    handleLayoutComplete = () => {
        if (!this.state.isLayoutComplete) {
            this.setState({
                isLayoutComplete: true
            });
        }
    };

    render() {
        const { myMedias } = this.props;
        const { isLayoutComplete } =  this.state;

        if (!myMedias) {
            return <div>loading...</div>
        }

        return <div>
            Фото
            <Masonry
                style={{visibility: isLayoutComplete ? 'visible' : 'hidden'}}
                className={'my-gallery-class'} // default ''
                elementType={'ul'} // default 'div'
                onLayoutComplete={this.handleLayoutComplete}
                options={{ transitionDuration: 0 }} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
            >
                    {this.renderItemList()}
            </Masonry>
            </div>
        ;
    }
}

export default ProfileMedia;
