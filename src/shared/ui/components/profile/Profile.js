import React, { Component } from 'react';
import ProfileAlbums from '../profileAlbums/ProfileAlbums';
import ProfileMedia from '../profileMedia/ProfileMedia';
import { fromUrlHelper } from '../../../utils/routingHelper';

class Profile extends Component {

    render() {
        const { myAlbums, myMedias, match } = this.props;
        const fromParam = (fromUrlHelper(match.url));

        return <div>
            <ProfileAlbums myAlbums={myAlbums} />
            <ProfileMedia myMedias={myMedias} fromParam={fromParam} />
        </div>
    }

}

export default Profile;