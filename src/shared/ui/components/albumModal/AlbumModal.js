import React, { Component } from 'react';
import qs from 'query-string';
import { urlFromHelper } from '../../../utils/routingHelper.js';
import Portal from '../portal/Portal';
import Items from './items';

class AlbumModal extends Component {

    componentDidMount() {
        this.portal.openPortal();
    }

    handleClose = e => {
        e.preventDefault();
        const search = qs.parse(this.props.location.search);

        if (search && search.from) {
            this.props.history.push(urlFromHelper(search.from));
        }
    };

    render() {
        const { album } = this.props;

        return (
            <Portal ref={portal => this.portal = portal}>
                <div className='modal'>
                    <div
                        className="ic-wrap-20-40 ** -sectPhoto-media"
                        onClick={this.handleClose}
                    >
                        <i className="ic-close size20 ** icon-close" />
                    </div>
                    {album && <Items medias={album.medias} />}
                </div>
            </Portal>
        );
    }
}

export default AlbumModal;
