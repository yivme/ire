import React, { Component } from 'react';
import Items from './Items';

class ProfileAlbum extends Component {
    render() {
        const { myAlbum } = this.props;

        if (!myAlbum) {
            return <div>loading...</div>
        }

        return <div>
            album_p {myAlbum.album.id}

                <Items medias={myAlbum.album.medias} />
            </div>
        ;
    }
}

export default ProfileAlbum;
