import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import { Link } from "react-router-dom";
import ImgListItem from '../imgListItem/ImgListItem';

class Items extends Component {

    constructor(props) {
        super(props);

        this.state = {
            built: false
        };
    }

    handleLayoutComplete = () => {
        const { built } = this.state;

        if (!built) {
            this.setState({
                built: true
            });
        }
    };

    render() {
        const { medias } = this.props;
        const { built } = this.state;

        return <Masonry
            style={{ visibility: built ? 'visible' : 'hidden' }}
            className={'my-gallery-class'} // default ''
            elementType={'ul'} // default 'div'
            onLayoutComplete={this.handleLayoutComplete}
            options={{ transitionDuration: 0 }} // default {}
            disableImagesLoaded={false} // default false
            updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
        >
            {medias.map(media => (
                <li key={media.id} style={{ padding: `10px` }} >
                    <Link to={`/album/${media.album_id}/${media.id}/?from=album`}>
                        <ImgListItem item={media} />
                    </Link>
                </li>
            ))}
        </Masonry>;
    }
}

export default Items;
