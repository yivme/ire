import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import { Link } from "react-router-dom";
import ImgListItem from '../imgListItem/ImgListItem';

class PhotosList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            built: false
        };
    }

    handleLayoutComplete = () => {
        const { built } = this.state;

        if (!built) {
            this.setState({
                built: true
            });
        }
    };

    renderList() {
        const { photos, fromParam } = this.props;
        const { built } = this.state;

        return <Masonry
            style={{ visibility: built ? 'visible' : 'hidden' }}
            className={'my-gallery-class'} // default ''
            elementType={'ul'} // default 'div'
            onLayoutComplete={this.handleLayoutComplete}
            options={{ transitionDuration: 0 }} // default {}
            disableImagesLoaded={false} // default false
            updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
        >
            {photos.media.map(item => {
                return <li key={item.id} style={{ padding: `10px` }}>
                    <Link to={`/album/${item.album_id}/${item.id}/?from=${fromParam}`}>
                        <ImgListItem item={item} />
                    </Link>
                </li>
            })}
        </Masonry>;
    }

    render() {
        const { photos } = this.props;
        let photosMarkup = 'loading...';

        if (photos) {
            photosMarkup = this.renderList();
        }

        return <div>
            photos_p
            <div style={{ padding: '20px' }}>Фотопоток</div>
            <ul style={{ display: 'flex' }}>
                <li style={{ padding: '20px' }}><Link to='/photos/popular/'>Популярные</Link></li>
                <li style={{ padding: '20px' }}><Link to='/photos/discussed/'>Обсуждаемые</Link></li>
                <li style={{ padding: '20px' }}><Link to='/photos/best/'>Фото дня</Link></li>
            </ul>

            {photosMarkup}
        </div>;
    }
}

export default PhotosList;
