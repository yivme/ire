import { matchPath } from "react-router-dom";
import { urlFromHelper } from '../shared/utils/routingHelper';
import routes, { getMediaItemByCurrentPath } from "../shared/navigate/routes";

function setRoute(route, params, store) {
    let acc = { actionsList: [], bundleName: null };
    const actionsList = route.component.initialAction(params);

    actionsList.forEach(action => {
        acc.actionsList.push(Promise.resolve(store.dispatch(action)));
    });
    acc.bundleName = route.bundleName;

    return acc;
}

export function preRender(req, store) {
    const mediaItemRoute = getMediaItemByCurrentPath(req.url);
    const urlFrom = urlFromHelper(req.query.from, req.url);

    if (mediaItemRoute && !urlFrom) {
        const albumRoute = routes.find(routeS => routeS.name === 'album');
        const mediaItemParams = matchPath(req.url, mediaItemRoute);

        return setRoute(albumRoute, { ...mediaItemParams, query: req.query }, store);
    }

    if (urlFrom) {
        let fromRouteParams;
        const fromRoute = routes.find(routeFrom => {
            fromRouteParams = matchPath(urlFrom, routeFrom);
            return fromRouteParams;
        });

        if (fromRouteParams) {
            return setRoute(fromRoute, { ...fromRouteParams, query: req.query }, store);
        }
    }

    return routes.reduce((acc, route) => {
        const routeParams = matchPath(req.url, route);

        if (routeParams && route.component && route.component.initialAction) {
            acc = setRoute(route, { ...routeParams, query: req.query }, store);
        }

        return acc;
    }, { actionsList: [], bundleName: null });
}
