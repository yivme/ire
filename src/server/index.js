import express from "express";
import cookieParser from 'cookie-parser';
import cors from "cors";
import React from "react";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import { StaticRouter } from "react-router-dom";
import serialize from "serialize-javascript";
import configureStore from "../shared/configureStore";
import { preRender } from './preRender';
import App from "../shared/App";
import "source-map-support/register";

const app = express();

const NODE_ENV = process.env.NODE_ENV;

app.use(cookieParser());
app.use(cors());
app.use(express.static("public"));

app.get("*", (req, res, next) => {
    const store = configureStore({ cookies: `PHPSESSID=${req.cookies.PHPSESSID}` });
    const acc = preRender(req, store);
    const bundleSRC = NODE_ENV === 'production'
        ? `/bundle.${acc.bundleName}.js`
        : `/bundle.js`
    ;

    Promise.all(acc.actionsList)
        .then(() => {
            const context = {};
            const markup = renderToString(
                <Provider store={store}>
                    <StaticRouter location={req.url} context={context}>
                        <App />
                    </StaticRouter>
                </Provider>
            );
            const initialData = store.getState();

            delete initialData.cookies;

            res.send(`
        <!DOCTYPE html>
        <html>
          <head>
            <title>W Combinator</title>
            <link rel="stylesheet" href="/css/main.css">
            <script src=${bundleSRC} defer></script>
            <script>window.__initialData__ = ${serialize(initialData)}</script>
          </head>

          <body>
            <div id="root">${markup}</div>
            <div class="region-modal"></div>
          </body>
        </html>
      `);
        })
        .catch(next);
});

app.listen(process.env.PORT || 3000, () => {
    console.log("Server is listening");
    console.log('NODE_ENV: ', process.env.NODE_ENV);
});
